G# BST: Bypass Simulation Toolset

This repository contains BST a NoC simulations toolset to simulate bypass routers. It is composed by four main elements:

1. An extended version of BookSim with single- and multi-hop bypass routers.
2. A customized version of OpenSMART to implement SMART++.
3. An API to easily integrate BookSim with other simulation tools. This repository includes an example of use for gem5.
4. Each of the tools included in BST has a variety of scripts to ease the generation of experiments, plots and debug.

## Installation
To install BST clone this repository and update its submodules:

```bash
git clone --recurse -submodules https://bitbucket.org/gicuc/bst.git
```

If you have clone the repository without the submodules you can use the following command:

```bash
git submodule init
git submodule update
```

## Tools

BST is composed by three submodules:

1. [booksim-unican](booksim-unican): BookSim extended with bypass routers.
2. [gem5_integration](gem5_integration): Contains the code and instructions to integrate BookSim in gem5.
3. [opensmart](opensmart): Contains the customized version of OpenSMART.

## License
The tools of this project are protected under the following licenses:

- **BST**: booksim-unican and gem5_integration
- **MIT**: opensmart

## Citation

Please, cite the following paper if you use this work.

```bibtex
@inproceedings{perez2020bst,
  title={BST: A BookSim-based toolset to simulate NoCs with single- and multi-hop bypass},
  author={P{\'e}rez, Iv{\'a}n and Vallejo, Enrique and Moret{\'o}, Miquel and Beivide, Ram{\'o}n},
  booktitle={2020 IEEE International Symposium on Performance Analysis of Systems and Software (ISPASS)},
  year={2020}
}
```

Also, you should mention the original publications of BookSim2 and OpenSMART.
